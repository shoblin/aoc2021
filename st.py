import fa
from solution import task_01, task_02, task_03, task_04, task_05
from solution import task_06, task_07, task_08, task_09, task_10
from solution import task_11


def start_task_01():
    file_path = 'puzzle_inputs/task_01'
    with open(file_path, 'r') as file:
        raw_puzzle_input = fa.convert_row_list(file)
        puzzle_input = fa.str_convert_int(raw_puzzle_input)

    solve_task_01(puzzle_input)


def start_task_02():
    file_path = 'puzzle_inputs/task_02'
    with open(file_path, 'r') as file:
        puzzle_input = fa.convert_row_list(file)
        commands = fa.split_str_in_list(puzzle_input)
    solve_task_02(commands)


def start_task_03():
    file_path = 'puzzle_inputs/task_03'
    with open(file_path, 'r') as file:
        puzzle_input = fa.convert_row_list(file)
    solve_task_03(puzzle_input)


def start_task_04():
    file_path = 'puzzle_inputs/task_04'
    with open(file_path, 'r') as file:
        numbers = file.readline()
        numbers = numbers.strip().split(",")
        puzzle_input = fa.convert_row_list(file)
        puzzle_input = fa.split_str_in_list(puzzle_input)
        boards = fa.create_bingo_boards(puzzle_input)

    solve_task_04(numbers, boards)


def start_task_05():
    file_path = 'puzzle_inputs/task_05'
    with open(file_path, 'r') as file:
        puzzle_input = fa.convert_row_list(file)
    line_coordinates = task_05.get_lines_coordinates(puzzle_input)
    board = task_05.create_board(line_coordinates)
    solve_task_05(line_coordinates, board)


def start_task_06():
    file_path = 'puzzle_inputs/task_06'
    with open(file_path, 'r') as file:
        puzzle_input = list(map(int, file.readline().split(",")))
    solve_task_06(puzzle_input)


def start_task_07():
    file_path = 'puzzle_inputs/task_07'
    with open(file_path, 'r') as file:
        puzzle_input = list(map(int, file.readline().split(",")))
    solve_task_07(puzzle_input)


def start_task_08():
    file_path = 'puzzle_inputs/task_08'
    with open(file_path, 'r') as file:
        puzzle_input = [line.strip().split(" | ") for line in file]
    solve_task_08(puzzle_input)


def start_task_09():
    file_path = 'puzzle_inputs/task_09'
    with open(file_path, 'r') as file:
        puzzle_input = [[int(sym) for sym in line.strip()] for line in file]
    board, max_x, max_y = task_09.create_board(puzzle_input)
    solve_task_09(board, max_x, max_y)


def start_task_10():
    file_path = 'puzzle_inputs/task_10'
    with open(file_path, 'r') as file:
        puzzle_input = [line.strip() for line in file]
    solve_task_10(puzzle_input)


def start_task_11():
    file_path = 'puzzle_inputs/task_11'
    with open(file_path, 'r') as file:
        puzzle_input = [line.strip() for line in file]
    puzzle_input = [[int(num) for num in line] for line in puzzle_input]
    solve_task_11(puzzle_input)


def solve_task(puzzle_input, module_name):
    f = getattr(module_name, "resolve_puzzle_part_1")(puzzle_input)
    print(f)
    print(task_01.resolve_puzzle_part_2(puzzle_input))


def solve_task_01(puzzle_input):
    print(task_01.resolve_puzzle_part_1(puzzle_input))
    print(task_01.resolve_puzzle_part_2(puzzle_input))


def solve_task_02(puzzle_input):
    print(task_02.resolve_puzzle_part_1(puzzle_input))
    print(task_02.resolve_puzzle_part_2(puzzle_input))


def solve_task_03(puzzle_input):
    print(task_03.resolve_puzzle_part_1(puzzle_input))
    print(task_03.resolve_puzzle_part_2(puzzle_input))


def solve_task_04(numbers, boards):
    # print(task_04.resolve_puzzle_part_1(numbers, boards))
    print(task_04.resolve_puzzle_part_2(numbers, boards))


def solve_task_05(line_coordinates, board):
    result_part1, board = task_05.resolve_puzzle_part_1(line_coordinates, board)
    print("Part 1: ", result_part1)
    print(task_05.resolve_puzzle_part_2(line_coordinates, board))


def solve_task_06(puzzle_input):
    print(task_06.resolve_puzzle_part_1(puzzle_input))
    print(task_06.resolve_puzzle_part_2(puzzle_input))


def solve_task_07(puzzle_input):
    print(task_07.resolve_puzzle_part_1(puzzle_input))
    print(task_07.resolve_puzzle_part_2(puzzle_input))


def solve_task_08(puzzle_input):
    print(task_08.resolve_puzzle_part_1(puzzle_input))
    print(task_08.resolve_puzzle_part_2(puzzle_input))


def solve_task_09(puzzle_input, max_x, max_y):
    print(task_09.resolve_puzzle_part_1(puzzle_input, max_x, max_y))
    print(task_09.resolve_puzzle_part_2(puzzle_input, max_x))


def solve_task_10(puzzle_input):
    result_part1, puzzle_input_part_2 = task_10.resolve_puzzle_part_1(puzzle_input)
    print('Part_1:', result_part1)
    print(task_10.resolve_puzzle_part_2(puzzle_input_part_2))


def solve_task_11(puzzle_input):
    print(task_11.resolve_puzzle_part_1(puzzle_input))
    print(task_11.resolve_puzzle_part_2(puzzle_input))


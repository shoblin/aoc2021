def check_board_line(line, number):
    return number in line


class BingoBoard:

    def __init__(self, number: int):
        self.name = f"Board_{number}"
        self.board = [[0] * 5] * 5
        self.fullness_col = [0] * 5
        self.fullness_row = [0] * 5
        self.fullness_dg = [0] * 2

        self.find_numbers = []

    @property
    def width(self):
        return len(self.board)

    @property
    def height(self):
        return len(self.board[0])

    @property
    def sum(self):
        board_sum = 0
        for line in self.board:
            for num in line:
                board_sum += int(num)
        return board_sum

    def __str__(self):
        result_str = f"== {self.name} ==\n"
        result_str += "\n".join([" ".join([str(number) for number in board_line]) for board_line in self.board])
        return result_str

    def __repr__(self):
        return self.name

    def set_numbers(self, board):
        self.board = board

    def check_board(self, number):
        for line in range(5):
            if check_board_line(self.board[line], number):
                column = self.board[line].index(number)
                self.find_numbers.append(number)
                self.fullness_col[column] += 1
                self.fullness_row[line] += 1
                if line == column:
                    self.fullness_dg[0] += 1
                elif line + column == 4:
                    self.fullness_dg[1] += 1
                # print(f"{self.board} : {line}, {column}")

    def check_win(self):
        return 5 in self.fullness_row or 5 in self.fullness_col or 5 in self.fullness_dg

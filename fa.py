from Bingo import bingo


def convert_row_list(file):
    return [line.strip() for line in file]


def str_convert_int(puzzle_inputs: list):
    return [int(line) for line in puzzle_inputs]


def split_str_in_list(puzzle_inputs: list):
    return [line.split() for line in puzzle_inputs]


def create_bingo_boards(puzzle_inputs: list):
    boards = []
    board = []
    board_num = 0
    for line in puzzle_inputs:
        if not line:
            new_board = bingo.BingoBoard(board_num)
            if board:
                new_board.set_numbers(board[:])
                boards.append(new_board)
            board = []
            board_num += 1
        else:
            board.append(line)

    return boards





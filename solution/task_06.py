
def count_fish(puzzle_input, days=80):
    fish_counter = [0] * 9
    for day in puzzle_input:
        fish_counter[day] +=1

    for _ in range(days):
        new_fish = fish_counter[0]
        fish_counter = fish_counter[1:] + fish_counter[:1]
        fish_counter[6] += new_fish
        # print(fish_counter)

    return sum(fish_counter)


def resolve_puzzle_part_1(puzzle_input):
    return count_fish(puzzle_input, days=80)


def resolve_puzzle_part_2(puzzle_input):
    return count_fish(puzzle_input, days=256)

import numpy as np
from scipy import ndimage


def check_cell(board, x, y):
    current_cell = board[y][x]
    if current_cell >= board[y-1][x]:
        return False
    if current_cell >= board[y+1][x]:
        return False
    if current_cell >= board[y][x-1]:
        return False
    if current_cell >= board[y][x+1]:
        return False
    return True


def create_board(puzzle_input):
    len_y = len(puzzle_input)
    len_x = len(puzzle_input[0])
    board = np.ones((len_y + 2, len_x + 2)) * 99
    for y in range(len_y):
        for x in range(len_x):
            board[y + 1][x + 1] = puzzle_input[y][x]

    return board, len_x, len_y


def resolve_puzzle_part_1(board, max_x, max_y):
    good_cells = []
    for y in range(1, max_y + 1):
        for x in range(1, max_x + 1):
            if check_cell(board, x, y):
                good_cells.append(board[y][x] + 1)

    return sum(good_cells)


def resolve_puzzle_part_2(puzzle_input, max_x):
    board = np.array(puzzle_input)
    board = board.reshape(-1, max_x)
    label, num_label = ndimage.label(board < 9)
    size = np.bincount(label.ravel())

    x, y = 0, 0
    first = []
    second = []
    third = []
    for line in label:
        for sym in line:
            if sym != 0:
                format_str = '1;31;46'
                if sym == 210:
                    first.append((y, x))
                    format_str = '1;31;43'
                elif sym == 200:
                    second.append((y, x))
                    format_str = '1;31;44'
                elif sym == 212:
                    third.append((y,x))
                    format_str = '1;31;47'
            else:
                format_str = '4;40;40'
            print(f'\x1b[{format_str}m{board[y][x]:1.0f}\x1b[0m', end=" ")
            x += 1
        print()
        x = 0
        y += 1

    spots = {i: size[i] for i in range(len(size))}
    spots = sorted(spots.items(), key=lambda kv: kv[1], reverse=True)
    print(len(first))
    print(len(second))
    print(len(third))
    # print(sorted(size[1:], reverse=True))
    # size = np.bincount(label.ravel())
    # spots = {i: size[i] for i in range(1, len(size)) }
    # spots = sorted(spots.items(), key=lambda kv: kv[1], reverse=True)
    # print(spots)
    #

    top3 = sorted(size[1:], reverse=True)[:3]
    print(np.prod(top3))

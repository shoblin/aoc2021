def resolve_puzzle_part_1(puzzle_input):
    result = 0
    for num in range(len(puzzle_input)):
        if puzzle_input[num - 1] < puzzle_input[num]:
            result += 1
    return result


def resolve_puzzle_part_2(puzzle_input):
    result: int = 0
    for num in range(len(puzzle_input) - 3):
        if sum(puzzle_input[num:num + 3]) < sum(puzzle_input[num + 1:num + 4]):
            result += 1
    return result

import time
from Clock_7_segment import clock


def resolve_puzzle_part_1(puzzle_input):
    i = 0
    for line in puzzle_input:
        for digit in line[1].split():
            if len(digit) in [2, 3, 4, 7]:
                i += 1

    return i


def resolve_puzzle_part_2(puzzle_input):
    output_sum = 0
    for line in puzzle_input:
        current_digit = ""
        one, four, seven, eight = find_pattern_1478(line[0])

        for seg in line[1].split():
            seg = set(seg)
            if len(seg) == 2:
                current_digit += "1"
            elif len(seg) == 3:
                current_digit += "7"
            elif len(seg) == 4:
                current_digit += "4"
            elif len(seg) == 7:
                current_digit += "8"
            elif len(seg) == 5 and len(seg & seven) == 3:
                current_digit += "3"
            elif len(seg) == 5 and len(seg & four) == 3:
                current_digit += "5"
            elif len(seg) == 5 and len(seg & four) == 2:
                current_digit += "2"
            elif len(seg) == 6 and len(seg & four) == 4:
                current_digit += "9"
            elif len(seg) == 6 and len(seg & four) == 3 and len(seg & one) == 2:
                current_digit += "0"
            else:
                current_digit += "6"

        output_sum += int(current_digit)
        clock.change_digit(current_digit)

    return output_sum


def find_pattern_1478(pattern_string: str):
    pattern_len = {len(pattern): set(pattern) for pattern in pattern_string.split()}
    return pattern_len[2], pattern_len[4], pattern_len[3], pattern_len[7]

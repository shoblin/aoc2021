# --------------- Part 1 ---------------

def resolve_puzzle_part_1(numbers: list, boards: list):
    win_board = find_win_board(numbers, boards)
    sum_find_num = sum(int(num) for num in win_board.find_numbers)
    return (win_board.sum - sum_find_num) * int(win_board.find_numbers[-1])


def find_win_board(numbers: list, boards: list):
    for rnd_number in numbers:
        for board in boards:
            board.check_board(rnd_number)
            if board.check_win():
                # print(f"WIN - last_number: {rnd_number}")
                # print(board)
                # print(board.find_numbers)
                return board


# --------------- Part 2 ---------------
def resolve_puzzle_part_2(numbers: list, boards: list):
    last_win_board = find_last_win_board(numbers, boards)
    print(last_win_board)
    sum_find_num = sum(int(num) for num in last_win_board.find_numbers)
    print(last_win_board.find_numbers[-1])
    print(last_win_board.sum)
    return (last_win_board.sum - sum_find_num) * int(last_win_board.find_numbers[-1])


def find_last_win_board(numbers: list, boards: list):

    for rnd_number in numbers:
        num_board = 0

        while len(boards) > num_board:

            board = boards[num_board]
            board.check_board(rnd_number)
            if board.check_win():
                boards.remove(board)

            num_board += 1

        if len(boards) == 1:
            return board

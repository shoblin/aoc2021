# --------------- Part 1 ---------------

def resolve_puzzle_part_1(puzzle_input: list):
    lines_num = len(puzzle_input)
    data = count_1_string(puzzle_input)
    print(data)
    gamma_rate, epsilon_rate = calc_rates(data, lines_num)
    gamma_rate = int(gamma_rate, 2)
    epsilon_rate = int(epsilon_rate, 2)

    return gamma_rate * epsilon_rate


def count_1_string(puzzle_input):
    line_len = len(puzzle_input[0])
    new_puzzle_input = [0 for _ in range(line_len)]
    for line in puzzle_input:
        for position in range(line_len):
            new_puzzle_input[position] += int(line[position])

    return new_puzzle_input


def calc_rates(data, line_numbers) -> tuple:
    gamma_rate = epsilon_rate = ""
    for rate in data:
        gamma_rate += "1" if rate > line_numbers / 2 else "0"
        epsilon_rate += "1" if rate < line_numbers / 2 else "0"

    return gamma_rate, epsilon_rate


# --------------- Part 2 ---------------
def resolve_puzzle_part_2(puzzle_input: list) -> int:
    puzzle_input = sorted(puzzle_input)
    oxy_rate = calc_rating(puzzle_input, "oxy")
    c02_rate = calc_rating(puzzle_input, "co2")

    return oxy_rate * c02_rate


def calc_rating(data: list, mode: str) -> int:
    position = 0
    while len(data) > 1:
        edge_position = count_digit_position(data, position)
        if mode == "oxy":
            data = get_most_common_data(data, edge_position)
        else:
            data = get_least_common_data(data, edge_position)

        position += 1
    return int(data[0], 2)


def count_digit_position(data: list, position: int) -> int:
    num = 0
    for line in data:
        if line[position] == "1":
            return num

        num += 1
    return num


def get_most_common_data(data: list, position: int) -> list:
    if len(data[:position]) > len(data[position:]):
        return data[:position]
    else:
        return data[position:]


def get_least_common_data(data: list, position: int) -> list:
    if len(data[position:]) < len(data[:position]):
        return data[position:]
    else:
        return data[:position]

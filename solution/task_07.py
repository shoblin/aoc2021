
def resolve_puzzle_part_1(puzzle_input):
    crab_max = max(puzzle_input)
    flue = []
    for position in range(crab_max):
        flue.append(sum([abs(position - crab_position) for crab_position in puzzle_input]))
    return min(flue)


def resolve_puzzle_part_2(puzzle_input):
    crab_max = max(puzzle_input)
    flue = []
    for position in range(crab_max):
        diff = [abs(position - crab_position) for crab_position in puzzle_input]
        flue.append(sum([sum(list(range(dy + 1))) for dy in diff]))
    return min(flue)
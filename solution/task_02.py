COMMANDS = {
            "forward": (1, 0),
            "up": (0, -1),
            "down": (0, 1)
            }


def calc_next_change(direction, steps):
    return direction[0] * steps, direction[1] * steps


def resolve_puzzle_part_1(puzzle_input: list):
    position = [0, 0]
    for command, steps in puzzle_input:
        dx, dy = calc_next_change(COMMANDS.get(command), int(steps))
        position[0] += dx
        position[1] += dy

    return position[0] * position[1]


def calc_change_aim(command, steps):
    return command[1] * int(steps)


def calc_next_step(steps, aim):
    return steps, aim * steps


def resolve_puzzle_part_2(puzzle_input: list):
    position = [0, 0]
    aim = 0
    for command, steps in puzzle_input:
        command = COMMANDS.get(command)
        steps = int(steps)
        if not command[0]:
            aim += calc_change_aim(command, steps)
        else:
            dx, dy = calc_next_step(steps, aim)
            position[0] += dx
            position[1] += dy

    return position[0] * position[1]
import numpy as np


def create_board(puzzle_input):
    return np.array(puzzle_input)


# --------------- Part 1 ---------------
def resolve_puzzle_part_1(puzzle_input):
    board = create_board(puzzle_input)
    flashes = 0
    for i in range(100):
        flashes += calculate_step(board)

    print(board)
    print(flashes)


def calculate_step(board):
    board += 1
    flashes = 0
    while True:
        masked = np.where(board > 9)
        num_flashes = len(masked[0])
        flashes += num_flashes
        if num_flashes == 0:
            return flashes
        upgrade_board_flashes(board, masked)


def upgrade_board_flashes(board, mask):
    adj = [(i, j) for i in range(-1, 2) for j in range(-1, 2) if i != 0 or j != 0]

    for crd in list(zip(mask[0], mask[1])):
        for d in adj:
            if 0 <= crd[0] + d[0] < 10 and 0 <= crd[1] + d[1] < 10:
                if board[crd[0] + d[0]][crd[1] + d[1]] != 0:
                    board[crd[0] + d[0]][crd[1] + d[1]] += 1
        board[crd[0]][crd[1]] = 0


# --------------- Part 2 ---------------
def resolve_puzzle_part_2(puzzle_input):
    board = create_board(puzzle_input)
    steps = 0
    while (board == 0).sum() < 100:
        calculate_step(board)
        steps += 1
    print(board)
    print(steps)

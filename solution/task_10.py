SYMBOLS = {
    "(": 3, ")": -3,
    "[": 57, "]": -57,
    "{": 1197, "}": -1197,
    "<": 25137, ">": -25137
}

SYMBOLS_2 = {
    "(": 1, ")": -1,
    "[": 2, "]": -2,
    "{": 3, "}": -3,
    "<": 4, ">": -4
}


# --------------- Part 1 ---------------
def resolve_puzzle_part_1(puzzle_input):
    bad_symbols = []
    correct_commands = []
    for line in puzzle_input:
        symbol = find_lost_symbols(line)
        if symbol:
            bad_symbols.append(symbol)
        else:
            correct_commands.append(line)
    return sum(bad_symbols), correct_commands


def find_lost_symbols(line):
    current_line = []
    for symbol in line:
        current_symbol = SYMBOLS[symbol]
        if current_symbol > 0:
            current_line.append(current_symbol)
        else:
            before_symbol = current_line.pop()
            if before_symbol + current_symbol != 0:
                return -1 * current_symbol

    return 0


# --------------- Part 2 ---------------
def resolve_puzzle_part_2(puzzle_input):
    scores = []
    for line in puzzle_input:
        lost_symbols = find_all_lost_symbols(line)
        scores.append(count_score(lost_symbols))
    num_middle_score = len(scores) // 2
    return sorted(scores)[num_middle_score]


def find_all_lost_symbols(line):
    lost_symbols = []
    current_line = []

    for symbol in line:
        current_symbol = SYMBOLS_2[symbol]
        if current_symbol > 0:
            current_line.append(current_symbol)
        else:
            before_symbol = current_line.pop()
            if before_symbol + current_symbol != 0:
                lost_symbols.append(before_symbol)

    current_line.reverse()

    return lost_symbols + current_line


def count_score(lost_symbols):
    total_score = 0
    for symbol in lost_symbols:
        total_score = total_score * 5 + symbol

    return total_score

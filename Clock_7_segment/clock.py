import os
import numpy as np
num_digits = 4
display = np.zeros((9, 8 * num_digits))

SYMBOLS = {0: ".", 1: "#"}


def set_1(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[1:4, num_digit + 6] = 1
    disp[5:8, num_digit + 6] = 1


def set_2(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 6] = 1
    disp[5:7, num_digit + 1] = 1


def set_3(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 6] = 1
    disp[5:7, num_digit + 6] = 1


def set_4(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[4, num_digit + 2: num_digit + 6] = 1
    disp[1:4, num_digit + 1] = 1
    disp[1:4, num_digit + 6] = 1
    disp[5:8, num_digit + 6] = 1


def set_5(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[5:7, num_digit + 6] = 1
    disp[2:4, num_digit + 1] = 1


def set_6(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 1] = 1
    disp[5:7, num_digit + 1] = 1
    disp[5:7, num_digit + 6] = 1


def set_7(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[1, num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 6] = 1
    disp[5:8, num_digit + 6] = 1


def set_8(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 1] = 1
    disp[2:4, num_digit + 6] = 1
    disp[5:7, num_digit + 1] = 1
    disp[5:7, num_digit + 6] = 1


def set_9(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 4, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:4, num_digit + 1] = 1
    disp[2:4, num_digit + 6] = 1
    disp[5:7, num_digit + 6] = 1


def set_0(disp, num_digit):
    num_digit = (num_digit - 1) * 8
    disp[(1, 7), num_digit + 2: num_digit + 6] = 1
    disp[2:7, num_digit + 1] = 1
    disp[2:7, num_digit + 6] = 1


FUNCTION = {
            1: set_1, 2: set_2, 3: set_3,
            4: set_4, 5: set_5, 6: set_6,
            7: set_7, 8: set_8, 9: set_9,
            0: set_0
}


def change_digit(digits):
    global display
    num_digit = 1
    for digit in digits:
        FUNCTION[int(digit)](display, num_digit)
        num_digit += 1
    print_display()


def print_display():

    for line in display:
        for x in line:
            print(SYMBOLS[x], end="")
        print()
    display[:, :] = 0


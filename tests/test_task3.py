import unittest
import fa
import solution.task_03 as task


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        with open("task3_test_inputs", "r") as file:
            self.puzzle_input = fa.convert_row_list(file)

    def test_count_1_string(self):
        right_counts = [7, 5, 8, 7, 5]
        self.assertEqual(right_counts, task.count_1_string(self.puzzle_input))

    def test_resolve_puzzle_part_1(self):
        data = [7, 5, 8, 7, 5]
        test_gamma, test_es = task.calc_rates(data, 12)
        self.assertEqual("10110", test_gamma)
        self.assertEqual("01001", test_es)

    # def test_get_most_common_data_0(self):
    #     puzzle_input = ["00100", "11110", "10110", "10111",
    #                     "10101", "01111", "00111", "11100",
    #                     "10000", "11001", "00010", "01010"]
    #     position = task.count_digit_position(data, 0)
    #     test_oxy = task.get_most_common_data(data, position)
    #     rdata = ["0000", "0001", "0010", "0101"]
    #     self.assertEqual(rdata, test_oxy)


if __name__ == '__main__':
    unittest.main()
